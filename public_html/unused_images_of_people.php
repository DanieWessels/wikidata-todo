<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');
ini_set('memory_limit','500M');

require_once ( 'php/ToolforgeCommon.php' ) ;
$tfc = new ToolforgeCommon('unused_images_of_people') ;

$depth = 2 ;

$db = $tfc->openDB ( 'commons' , 'wikimedia' ) ;

$cats = [] ;
$tfc->findSubcats ( $db , array('People') , $cats , $depth ) ;

print $tfc->getCommonHeader ( '' , 'Unused images of people' ) ;
#print "<pre>" ; print_r ( $cats ) ; print "</pre>" ; 

$sql = "SELECT DISTINCT page_title FROM page,categorylinks WHERE cl_from=page_id AND page_namespace=6 AND cl_to IN ('" . implode("','",$cats) . "')" ;
$sql .= " AND NOT EXISTS (SELECT * FROM globalimagelinks WHERE page_title=gil_to LIMIT 1)" ;
$sql .= " AND page_is_redirect=0" ;
$sql .= " LIMIT 100" ;

$result = $tfc->getSQL ( $db , $sql ) ;
while($o = $result->fetch_object()){
	print "<p>" . $o->page_title . "</p>" ;
//	$ret[$o->page_title] = $o->page_title ;
}


?>