<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');
ini_set('memory_limit','2500M');
set_time_limit ( 60 * 10 ) ; // Seconds
include_once ( "php/common.php" ) ;

$site = trim ( strtolower ( get_request ( 'site' , 'enwiki' ) ) ) ;
$limit = get_request ( 'limit' , 0 ) * 1 ;
$testing = isset($_REQUEST['test']) ;

print get_common_header ( '' , 'Wikidata duplicate item finder' ) ;
print "
<div class='lead'>This tool finds potential duplicate items on Wikidata, via identical labels/aliases.</div>
<form method='get' class='form-inline'>
<div>Site: <input type='text' value='$site' name='site' /><input type='submit' name='run' value='Do it' class='btn btn-outline-primary' /></div>
<div style='display:block'>Limit: <input name='limit' type='number' placeholder='e.g. 1000' /> (optional, for testing; number of initial candidates, not necessarily results)</div>
</form>
<div>
<i>Note:</i> For large wikis such as enwiki, this will take several minutes to run. Please be patient and do not hit reload in vain!
</div>
" ;

if ( !isset($_REQUEST['run']) ) {
	print get_common_footer() ;
	exit ( 0 ) ;
}

$db = openDB ( 'wikidata' , 'wikidata' ) ;
$site = $db->real_escape_string ( $site ) ;
$wp_types = "'Q4167410','Q13406463','Q4167836','Q11266439'" ;

$sql = "select ips_item_id,group_concat(ips_site_id separator '|') AS sites,group_concat(ips_site_page separator '|') AS pages,count(*) AS cnt from wb_items_per_site  group by ips_item_id having cnt=1 and sites='$site'" ;
if ( $limit > 0 ) $sql .= " LIMIT $limit" ;

$items = array() ;
$result = getSQL ( $db , $sql ) ;
while($o = $result->fetch_object()){
	if ( preg_match ( '/:/' , $o->pages ) ) continue ;
	$items[$o->ips_item_id] = preg_replace ( '/\s+\(.+\)$/' , '' , $o->pages ) ;
}

if ( $testing ) {
	print "<pre>" ; print_r ( $items ) ; print "</pre>" ;
}

// Remove disambig etc.
//$sql = "select distinct epp_entity_id from pagelinks,wb_entity_per_page WHERE epp_entity_id IN (" . implode(',',array_keys($items)) . ") AND epp_entity_type='item' and epp_page_id=pl_from AND pl_namespace=0 AND pl_title IN ($wp_types)" ;
$sql = "select distinct page_title from pagelinks,page WHERE page_namespace=0 AND page_title IN ('Q" . implode("','Q",array_keys($items)) . "') and page_id=pl_from AND pl_namespace=0 AND pl_title IN ($wp_types)" ;
if ( $testing ) {
	print "<pre>" ; print $sql ; print "</pre>" ;
}
$result = getSQL ( $db , $sql ) ;
while($o = $result->fetch_object()){
	$tmp_q = preg_replace ( '/\D/' , '' , $o->page_title ) ;
	if ( $testing ) print "Removing $tmp_q<br/>" ;
	unset ( $items[$tmp_q] ) ;
}

print "<div>Found " . number_format(count($items),0) . " items unique to $site.</div>" ;
myflush() ;


$had_label = array() ;
print "<ol>" ;
foreach ( $items AS $q => $page ) {
	$s = $db->real_escape_string ( $page ) ;
	if ( isset ( $had_label[$s] ) ) continue ;
	$had_label[$s] = 1 ;
	$sql = "select DISTINCT term_full_entity_id from wb_terms where term_type IN ('label','alias') AND term_text='$s' AND term_full_entity_id!='Q{$q}'" ;
	$sql .= " AND NOT EXISTS (SELECT * FROM  pagelinks,page WHERE page_namespace=0 AND term_full_entity_id=page_title and page_id=pl_from AND pl_namespace=0 AND pl_title IN ($wp_types))" ; // Disambig etc
	$sql .= " AND NOT EXISTS (SELECT * FROM wb_items_per_site WHERE concat('Q',ips_item_id)=term_full_entity_id AND ips_site_id='$site')" ; // No link to same wiki
	$result = getSQL ( $db , $sql ) ;
	$found = array() ;
	while($o = $result->fetch_object()){
		$qq = $o->term_full_entity_id ;
		$found[] = "<a target='_blank' href='//www.wikidata.org/wiki/$qq'>$qq</a>" ;
	}
	if ( count ( $found ) == 0 ) continue ;
	print "<li>" ;
	print "Item <a href='//www.wikidata.org/wiki/Q$q' target='_blank'>Q$q</a> (<i>$page</i>) has potential duplicates: " ;
	print implode ( ", " , $found ) ;
	print "</li>" ;
	myflush() ;
}
print "</ol>" ;

print get_common_footer() ;

?>