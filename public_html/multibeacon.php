<?PHP

require_once ( 'php/ToolforgeCommon.php' ) ;

$tfc = new ToolforgeCommon() ;

$keyprop = $tfc->getRequest ( 'keyprop' , '' ) ;
$values = $tfc->getRequest ( 'values' , '' ) ;
$columns = $tfc->getRequest ( 'columns' , 'Q' ) ;

print $tfc->getCommonHeader ( 'Multi-BEACON' ) ;

print "<div class='lead'>Give a (string/ID) property and list of values, and generate a table of values for other properties (use \"Q\" for the Wikidata item).</div>" ;
print "<div><form class='form' method='post'>
<h3>Key property</h3>
<p><input type='text' name='keyprop' value='$keyprop' /></p>
<h3>Values for key property</h3>
<p><textarea style='width:100%' rows='5' name='values'>$values</textarea></p>
<p>One per line</p>

<h3>Columns</h3>
<p><textarea style='width:100%' rows='3' name='columns'>$columns</textarea></p>
<p>One Wikidata property (Pxxx) per line. Key property will automatically become first column.</p>

<p><input type='submit' name='doit' value='Do it!' class='btn btn-primary' /></p>
</form></div>" ;

if ( $keyprop != '' and $columns != '' ) {
	$keyprop = preg_replace ( '/\D/' , '' , $keyprop ) ;

	$columns = explode ( "\n" , $columns ) ;
	$columns2 = [] ;
	$columns2[] = [ "P$keyprop" , $keyprop ] ;
	$props = [$keyprop] ;
	$prop2col = [] ;
	foreach ( $columns AS $c ) {
		$c = trim($c) ;
		if ( preg_match ( '/^[pP]{0,1}(\d+)$/' , $c , $m ) ) {
			$props[] = $m[1] ;
			$prop2col[$m[1]] = count($columns2) ;
			$columns2[] = [$c , $m[1]] ;
		}
	}

	$header = [ 'Item' ] ;
	foreach ( $columns2 AS $c ) $header[] = $c[0] ;
	
	$out = [] ;
	$out[] = implode ( "\t" , $header ) ;
	$values = explode ( "\n" , $values ) ;
	foreach ( $values AS $v ) {
		$v = trim ( $v ) ;
		$sparql = "SELECT ?q" ;
		foreach ( $props AS $p ) $sparql .= " ?p{$p}" ;
		$sparql .= " { ?q wdt:P{$keyprop} \"{$v}\"" ;
		foreach ( $props AS $p ) $sparql .= " OPTIONAL { ?q wdt:P{$p} ?p{$p} }" ;
		$sparql .= " }" ;
		$j = $tfc->getSPARQL ( $sparql ) ;

		$cnt = 0 ;
		foreach ( $j->results->bindings AS $b ) {
			$q = $tfc->parseItemFromURL ( $b->q->value ) ;
			$o = [ $q ] ;
			foreach ( $columns2 AS $c ) {
				$p = 'p' . preg_replace('/\D/','',$c[0]) ;
#				print "<pre>{$p}!\n" ; print_r ( $b ) ; print "</pre>" ;		
				if ( isset($b->$p) ) {
					if ( $b->$p->type == 'uri' and preg_match ('|^http://www\.wikidata\.org/entity/|',$b->$p->value) ) {
						$o[] = $tfc->parseItemFromURL ( $b->$p->value ) ;
					} else $o[] = $b->$p->value ;
				} else $o[] = '' ;
			}
			$out[] = implode ( "\t" , $o ) ;
			$cnt++ ;
		}
		if ( $cnt == 0 ) $out[] = $v ;
		
	}
	
	print "<h3>Results</h3>" ;
	print "<textarea style='width:100%' rows=10>" ;
	print implode ( "\n" , $out ) ;
	print "</textarea>" ;
	
#	print "<pre>" ; print_r ( $columns2 ) ; print "</pre>" ;
}

print $tfc->getCommonFooter() ;

?>