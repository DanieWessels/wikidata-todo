<?PHP
error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once ( 'php/common.php' ) ;
require_once ( 'php/wikidata.php' ) ;

function parseDate ( $i , $p ) {
	$a = $i->getClaims ( $p ) ;
	if ( count ( $a ) == 0 ) return '' ;
	$v = $a[0]->mainsnak->datavalue->value ;
	if ( !preg_match ( '/^\+0*(\d+)-(\d\d)-(\d\d)T/' , $v->time , $m ) ) return '' ;
	if ( $v->precision >= 11 ) return $m[1].'-'.$m[2].'-'.$m[3] ;
	if ( $v->precision == 10 ) return $m[1].'-'.$m[2] ;
	if ( $v->precision ==  9 ) return $m[1] ;
	return '' ;
}

function getTargetLabel ( $i , $p ) {
	global $wil ;
	$a = $i->getClaims ( $p ) ;
	if ( count ( $a ) != 1 ) return '' ;
	$q = $i->getTarget ( $a[0] ) ;
	$wil->loadItem($q) ;
	$i2 = $wil->getItem($q) ;
//	$page = $i2->getSitelink('enwiki') ;
	$label = $i2->getLabel() ;
	return $label ;
}

function getNationality ( $i ) {
	global $wil ;
	$a = $i->getClaims ( 'P27' ) ;
	if ( count ( $a ) == 0 ) return '' ;
	$qs = array() ;
	foreach ( $a AS $claim ) $qs[] = $i->getTarget ( $claim ) ;
	$wil->loadItems ( $qs ) ;
	$ret = array() ;
	
	foreach ( $qs AS $q ) {
		$i2 = $wil->getItem ( $q ) ;
		$b = $i2->getClaims ( 'P297' ) ;
		if ( count($b) == 0 ) continue ;
		$ret[] = $b[0]->mainsnak->datavalue->value ;
	}
	return implode ( '/' , $ret ) ;
}



$wil = new WikidataItemList ;

$q = get_request ( 'q' , '' ) ;
$wil->sanitizeQ ( $q ) ;
if ( $q == 'Q' ) $q = '' ;


print get_common_header ( '' , 'Creator from Wikidata' ) ;
print "<div class='lead'>This tool takes a Wikidata ID and generates a Commons Creator template.</div>
<div style='color:red;font-size:+1'>Use of this tool to fill in Creator templates is <a href='https://commons.wikimedia.org/wiki/Template_talk:Creator#Recommendations_for_new_templates' target='_blank'>not recommended</a> anymore!</div>
<div><form method='get' action='?' class='form form-inline'>
Wikidata ID <input type='text' id='q' name='q' value='$q' /> <input type='submit' value='Generate template' class='btn btn-primary' />
</form></div>
<script>$(document).ready(function(){ $('#q').focus()})</script>" ;

if ( $q == '' ) {
	print get_common_footer() ;
	exit ( 0 ) ;
}

print "<hr/>" ;

$wil->loadItem ( $q ) ;

$t = array() ;
$i = $wil->getItem($q) ;
#print "<pre>" ; print_r ( $i ) ; print "</pre>" ;

$lnl = array() ; // Labels'n'links
if ( isset($i->j->sitelinks) ) {
	foreach ( $i->j->sitelinks AS $wiki => $v ) {
		if ( $wiki == 'commonswiki' or $wiki == 'wikidatawiki' ) continue ;
		if ( !preg_match ( '/^(.+)wiki$/' , $wiki , $m ) ) continue ;
		$l = $m[1] ;
		$lnl[$l] = array ( 'label' => $v->title , 'page' => $v->title ) ;
	}
}
if ( isset($i->j->labels) ) {
	foreach ( $i->j->labels AS $l => $v ) {
		$lnl[$l]['label'] = $v->value ;
	}
}

$t['Name'] = '' ;
if ( count($lnl) > 0 ) {
	$s = '{{LangSwitch' ;
	foreach ( $lnl AS $l => $v ) {
		$label = $v['label'] ;
		if ( isset($v['page']) ) {
			$s .= "\n    | $l = [[:$l:" . $v['page'] . "|$label]]" ;
		} else {
			$s .= "\n    | $l = $label" ;
		}
	}
	$s .= "\n  }}" ;
	$t['Name'] = $s ;
}

$t['Alternative names'] = array() ;
if ( isset ( $i->j->aliases ) ) {
	foreach ( $i->j->aliases AS $v0 ) {
		foreach ( $v0 AS $v ) $t['Alternative names'][$v->value] = $v->value ;
	}
}
$t['Alternative names'] = implode ( '; ' , $t['Alternative names'] ) ;

$t['Sortkey'] = '' ;
if ( preg_match ( '/^(.+) (\S+)$/' , $i->getLabel() , $m ) ) $t['Sortkey'] = $m[2] . ', ' . $m[1] ;

$t['Birthdate'] = parseDate ( $i , 'P569' ) ;
$t['Deathdate'] = parseDate ( $i , 'P570' ) ;
$t['Birthloc'] = getTargetLabel ( $i , 'P19' ) ;
$t['Deathloc'] = getTargetLabel ( $i , 'P20' ) ;
$t['Nationality'] = getNationality ( $i ) ;
$t['Gender'] = getTargetLabel ( $i , 'P21' ) ;
$t['Occupation'] = getTargetLabel ( $i , 'P106' ) ;
$t['Description'] = $i->getDesc() ;

$t['Homecat'] = '{{subst:PAGENAME}}' ;
$t['Option'] = '{{{1|}}} <!-- Do not modify -->' ;
 
/* TODO:
Wikisource
Wikiquote
*/

$t['Wikidata'] = $q ;
$t['Authority'] = "{{Authority control|q=$q|bare=1}}" ;
$t['Linkback'] = "Creator:" . $i->getLabel() ;

$t['Image'] = '' ;
$a = $i->getClaims ( 'P18' ) ;
if ( count($a) > 0 ) {
	$t['Image'] = $a[0]->mainsnak->datavalue->value ;
}

//print "<pre>" ; print_r ( $lnl ) ; print "</pre>" ;

# New style
$t = array (
	'Option' => '{{{1|}}} <!-- Do not modify -->' ,
	'Wikidata' => $q
) ;


print "<div><textarea style='width:100%' rows=15>
{{Creator\n" ;
foreach ( $t AS $k => $v ) {
	print " | $k = $v\n" ;
}
print "}}</textarea></div>" ;

print "<p>Copy the above text, then <a href='//commons.wikimedia.org/w/index.php?title=Creator:".myurlencode($i->getLabel())."&action=edit'>paste here</a>.</p>" ;

print get_common_footer() ;

?>