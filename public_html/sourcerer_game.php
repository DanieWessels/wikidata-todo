<?PHP

require_once ( 'php/common.php' ) ;

function getLabel ( $j , $q ) {
	$label = $q ;
	$langs = array ( 'en' , 'de' , 'es' , 'it' , 'fr' ) ;
	foreach ( $langs AS $lang ) {
		if ( !isset($j->entities->$q->labels->$lang) ) continue ;
		$label = $j->entities->$q->labels->$lang->value ;
		break ;
	}
	return $label ;
}



$db = openToolDB ( 'sourcerer_p' ) ;
$db->set_charset ( 'utf8' ) ;

if ( isset($_REQUEST['set_done']) ) {
	$sid = get_request ( 'sid' , '' ) ;
	$text = get_request ( 'text' , '' ) ;
	$sql = "UPDATE suggestions SET status=1 WHERE statement_id='" . $db->real_escape_string($sid) . "' AND statement_text='" . $db->real_escape_string($text) . "' AND status=0" ;
	$result = getSQL ( $db , $sql ) ;
	header("Content-Type:text/plain");
	print "{}" ;
	exit ( 0 ) ;
}


print get_common_header ( '' , 'Sourcerer game' ) ;
print "<div style='float:right'><span id='widar'></span></div>" ;

while ( 1 ) {
	$q = '' ;
	$sid = '' ;
	$st = '' ;
	while ( $q == '' ) {
		$r = rand()/getrandmax() ;
		$sql = "select * from suggestions where rand>=$r and status=0 order by rand asc limit 1" ;
		if ( isset($_REQUEST['sid']) ) $sql = "select * from suggestions WHERE statement_id='" . $db->real_escape_string(get_request('sid','')) . "'" ;
		$result = getSQL ( $db , $sql ) ;
		while($o = $result->fetch_object()){
			$q = $o->q ;
			$sid = $o->statement_id ;
			$st = $o->statement_text ;
		}
	}
	
	$list = array() ;
	$sql = "select * from suggestions where q=$q and statement_id='$sid' and statement_text='" . $db->real_escape_string($st) . "'" ;
	$result = getSQL ( $db , $sql ) ;
	while($o = $result->fetch_object()){
		$list[$o->source_url][] = $o ;
	}

	$url = "https://www.wikidata.org/w/api.php?action=wbgetentities&ids=Q$q&format=json" ;
	$j = json_decode ( file_get_contents ( $url ) ) ;


	$q2 = "Q$q" ;
	$label = getLabel ( $j , $q2 ) ;

	$prop = '' ;
	foreach ( $j->entities->$q2->claims AS $p => $arr ) {
		foreach ( $arr AS $c ) {
			if ( $c->id != $sid ) continue ;
			$prop = $p ;
			break ;
		}
		if ( $prop != '' ) break ;
	}


	if ( $prop == '' ) continue ;
	$url = "https://www.wikidata.org/w/api.php?action=wbgetentities&ids=$prop&format=json" ;
	$jp = json_decode ( file_get_contents ( $url ) ) ;
	
	$plabel = getLabel ( $jp , $prop ) ;

	$fin_buttons = '' ;
	$fin_buttons .= "<div style='float:right'>" ;
	$fin_buttons .= "<button class='btn btn-primary set_done' sid='" . $sid . "' text='" . htmlspecialchars($st, ENT_QUOTES, "UTF-8") . "'>Done</button>&nbsp;" ;
	$fin_buttons .= "<button class='btn btn-warning skip'>Skip</button>" ;
	$fin_buttons .= "</div>" ;


	print "<div class='lead'><h1>$label <small><a href='//www.wikidata.org/wiki/$q2' target='_blank'>$q2</a></small></h1><h2>$plabel <small>$prop</small> : " . $st . "</h2></div>" ;
	print $fin_buttons ;
	print "<p>Which of these sources support the above claim?</p>" ;
	
	print "<div><table class='table table-striped'>" ;
	print "<thead><tr><th>Source</th><th>Support</th></tr></thead><tbody>" ;
	foreach ( $list AS $url => $sublist ) {
		$server = $url ;
		if ( preg_match ( '/:\/\/(.+?)\//' , $url , $m ) ) $server = $m[1] ;
		$url_enc = htmlspecialchars($url, ENT_QUOTES, "UTF-8") ;
		print "<tr>" ;
		print "<td>" ;
		print "<div><a href='$url_enc' target='_blank'>$server</a> <small>$url</small></div>" ;
		$date = '' ;
		foreach ( $sublist AS $l ) {
			print "<div style='font-size:14pt;margin-bottom:10px;'>" ;
			print "..." . $l->source_blurb . "..." ;
			print "</div>" ;
			$date = $l->source_date ;
		}
		print "</td>" ;
		
		print "<td style='vertical-align:top'>" ;
		print "<button class='btn btn-info add_source' sid='$sid' url='$url_enc' date='$date' q='Q$q'>Add&nbsp;as&nbsp;source</button>" ;
		print "</td>" ;
		
		print "</tr>" ;
	}
	print "</tbody></table>" ;

	print $fin_buttons ;

#	print "<pre>" ; print_r ( $list ) ; print "</pre>" ;
	break ;
}

?>

<script>
$(document).ready ( function () {

	var widar_api = '/api.php' ;
	
	function get_snaks ( url , date ) {
		var year = date.substr ( 0 , 4 ) ;
		var month = date.substr ( 4 , 2 ) ;
		var day = date.substr ( 6 , 2 ) ;
		var snaks = {
			'P854' : [{
				snaktype:"value",
				property:'P854',
				datatype:'url',
				datavalue:{
					type:"string",
					value:url
				}
			}] ,
			'P813' : [{
				snaktype:"value",
				property:'P813',
				datatype:'time',
				datavalue:{
					type:"time",
					value:{
						after:0,
						before:0,
						timezone:0,
						calendarmodel:'http://www.wikidata.org/entity/Q1985727',
						precision:11,
						time:"+0000000"+year+"-"+month+"-"+day+"T00:00:00Z"
					}
				}
			}]
		} ;
		return snaks ;
	}

	function getWidar ( params , callback ) {
		$.get ( widar_api , params , function ( d ) {
			if ( d.error != 'OK' ) {
				console.log ( params ) ;
				if ( null != d.error.match(/Invalid token/) || null != d.error.match(/happen/) || null != d.error.match(/Problem creating item/) || ( params.action!='create_redirect' && null != d.error.match(/failed/) ) ) {
					console.log ( "ERROR (re-trying)" , params , d ) ;
					setTimeout ( function () { getWidar ( params , callback ) } , 500 ) ; // Again
				} else {
					console.log ( "ERROR (aborting)" , params , d ) ;
					var h = "<li style='color:red'>ERROR (" + params.action + ") : " + d.error + "</li>" ;
					$('#out ol').append(h) ;
					callback ( d ) ; // Continue anyway
				}
			} else {
				callback ( d ) ;
			}
		} , 'json' ) . fail(function() {
			console.log ( "Again" , params ) ;
			getWidar ( params , callback ) ;
		}) ;
	}
	
	var my_root = "/wikidata-todo/sourcerer_game.php" ;
	
	$('button.set_done').click ( function () {
		var o = $(this) ;
		var sid = o.attr('sid') ;
		var text = o.attr('text') ;
		$.post ( my_root , {
			set_done:1,
			sid:sid,
			text:text
		} , function () {
			window.location.href = my_root ;
		} , 'json' ) ;
	} ) ;

	$('button.skip').click ( function () {
		window.location.href = my_root ;
	} ) ;

	$('button.add_source').click ( function () {
		var o = $(this) ;
		var q = o.attr('q') ;
		var sid = o.attr('sid') ;
		var date = o.attr('date') ;
		var url = o.attr('url') ;
		
		var snaks = get_snaks ( url , date ) ;

		getWidar ( {
				action:'add_source',
				statement:sid,
				snaks:JSON.stringify(snaks),
				botmode:1
			} , function ( d ) {
			
				var h ;
				if ( d.error == 'OK' ) {
					h = "<div class='alert alert-success' role='alert'>Source&nbsp;added!</div>" ;
				} else {
					h = "<div class='alert alert-danger' role='alert'>" + d.error + "</div>" ;
				}
				o.replaceWith ( h ) ;
			
			} ) ;
	} ) ;
	
	
	$('#toolbar-right').prepend ( "<li id='widar'></li>" ) ;
	getWidar ( { action:'get_rights',botmode:1} , function ( d ) {
		if ( typeof d.result != 'undefined' &&  typeof d.result.query != 'undefined' &&  typeof d.result.query.userinfo != 'undefined' &&  typeof d.result.query.userinfo.name != 'undefined' ) {
			$('#widar').html ( "Welcome, " + d.result.query.userinfo.name + "!" ) ;
		} else {
			$('#widar').html ( "<a href='/api.php?action=authorize&path=sourcerer_game.php'>Log in</a> to add sources!" ) ;
		}
	} ) ;
	
} ) ;
</script>

<?PHP

print get_common_footer() ;

?>