<?PHP

//error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
//ini_set('display_errors', 'On');
ini_set('memory_limit','1500M');
set_time_limit ( 60 * 30 ) ; // Seconds

require_once ( 'php/common.php' ) ;

header('Content-type: application/json');

$db = openDB ( 'wikidata' , '' ) ;

$callback = get_request ( 'callback' , '' ) ;
$date = get_request ( 'date' , '' ) ;
$until = get_request ( 'until' , '' ) ;

$date = preg_replace ( '/\D/' , '' , $date ) ;
$until = preg_replace ( '/\D/' , '' , $until ) ;


$sql = "SELECT DISTINCT page_title FROM page WHERE page_namespace=0 AND page_id IN ( SELECT rev_page FROM revision WHERE rev_timestamp >= '$date'" ;
if ( $until != '' ) $sql .= " AND rev_timestamp <= '$until'" ;
$sql .= ")" ;

if ( isset ( $_REQUEST['deleted'] ) ) {
	$sql = "select distinct log_title as page_title from logging where log_type='delete' and log_action='delete' and log_namespace=0 and log_timestamp>='$date'" ;
	if ( $until != '' ) $sql .= " AND log_timestamp <= '$until'" ;
	$sql .= " union select page_title from page where page_namespace=0 and page_is_redirect=1 and page_links_updated>='$date'" ;
	if ( $until != '' ) $sql .= " AND page_links_updated <= '$until'" ;
}

$first = true ;
if ( $callback != '' ) print $callback . '(' ;
$result = getSQL ( $db , $sql ) ;
print "[" ;
while($r = $result->fetch_object()){
	if ( $first ) $first = false ;
	else print "," ;
	print '"' . $r->page_title . '"' ;
//	$d[] = $r->page_title ;
}
print "]" ;
if ( $callback != '' ) print ')' ;

?>