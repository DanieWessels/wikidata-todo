#!/usr/bin/php
<?PHP

$version = 2 ;
ini_set('user_agent','Magnus labs tools'); # Fake user agent

require_once ( '/data/project/wikidata-todo/public_html/php/wikidata.php' ) ;
require_once ( '/data/project/commons-delinquent/vendor/mediawiki-api/vendor/autoload.php' ) ;

$had_cid = array() ; // Cache of claim IDs processed

function addSource ( $i , $cid , $source ) {
	global $sources , $had_cid ;
	if ( count ( $source ) == 0 ) return ;
	
	if ( isset($had_cid[$cid]) ) return ;
	$had_cid[$cid] = 1 ;
	
	$claim = $i->getClaimByID ( $cid ) ;
	if ( !isset($claim) ) return ; // No such claim
	if ( isset($claim->references) ) {
		$p248 = '' ;
		foreach ( $source AS $s ) {
			if ( $s['prop'] == 'P248' ) $p248 = preg_replace ( '/\D/' , '' , $s['value'] ) ;
		}
		$numid = 'numeric-id' ;
		foreach ( $claim->references AS $dummy => $refgroup ) {
			if ( !isset($refgroup->snaks) ) continue ;
			if ( !isset($refgroup->snaks->P248) ) continue ;
			foreach ( $refgroup->snaks->P248 AS $dummy2 => $v2 ) {
				if ( !isset($v2->datavalue) ) continue ;
				if ( !isset($v2->datavalue->value) ) continue ;
				if ( !isset($v2->datavalue->value->$numid) ) continue ;
				if ( $v2->datavalue->value->$numid == $p248 ) { // Already has a reference to this source
					print "SKIPPING $cid\n" ;
					return ;
				}
			}
		}
	}
	
	print "Processing $cid\n" ;
	
	$new_source = array(
		'item' => $i->q ,
		'claim' => $cid ,
		'snaks' => (object) array()
	) ;
	foreach ( $source as $s ) {
		$p = $s['prop'] ;
		$d = (object) array(
			'snaktype'=>'value' ,
			'property' => $p ,
			'datatype' => $s['type']
		) ;

		if ( $s['type'] == 'time' ) {
			$d->datavalue = (object) array (
				'type' => "time",
				'value' => (object) array (
					'after'=>0,
					'before'=>0,
					'timezone'=>0,
					'calendarmodel'=>'http://www.wikidata.org/entity/Q1985727',
					'precision'=>$s['precision']*1,
					'time'=>$s['value'].''
				)
			) ;
		} else if ( $s['type'] == 'wikibase-item' ) {
			$d->datavalue = (object) array (
				'type' => 'wikibase-entityid' ,
				'value' => (object) array (
					"entity-type" => "item",
					"numeric-id" => preg_replace('/\D/','',$s['value'])
				)
			) ;
		} else if ( $s['type'] == 'string' ) {
			$d->datavalue = (object) array (
				'type' => "string",
				'value' => $s['value']
			) ;
		}
		if ( !isset($new_source['snaks']->$p) ) $new_source['snaks']->$p = array() ;
		array_push ( $new_source['snaks']->$p , $d ) ;
	}
	$sources[] = $new_source ;
}

function now() {
	$p1 = date ( "+0000000Y-m-d" ) ;
	$p2 = date ( "00:00:00" ) ;
	return $p1.'T'.$p2.'Z' ;
}

function tryStringValues ( $claims , $i , $label , $this_source ) {
	if ( trim($label) == '' ) return ; // Paranoia
	foreach ( $claims AS $c ) {
		$q = $i->getTarget ( $c ) ;
		if ( $q === false ) continue ;
		$i2 = new WDI ( $q ) ;
		if ( !$i2->hasLabel($label) ) continue ;
		addSource ( $i , $c->id , $this_source ) ;
	}
}

function isDateSubset ( $claim , $d ) {
	if ( !isset($claim->mainsnak) ) return false ;
	if ( !isset($claim->mainsnak->datavalue) ) return false ;
	if ( !isset($claim->mainsnak->datavalue->value) ) return false ;
	$v = $claim->mainsnak->datavalue->value ;
	if ( !isset($v->time) ) return false ;
	if ( $v->calendarmodel != 'http://www.wikidata.org/entity/Q1985727' ) { // Not Gregorian calendar
		print "Using different calendar {$v->calendarmodel}\n" ;
		return false ;
	}
	if ( !preg_match ( '/^.0*(\d+)-(\d+)-(\d+)T/' , $v->time , $m ) ) return ;
	$w = array ( 'year' => $m[1]*1 , 'month' => $m[2]*1 , 'day' => $m[3]*1 , 'precision' => $v->precision ) ;

	if ( $w['precision'] > $d['precision'] ) return false ;
	if ( $w['precision'] < 9 ) return false ; // Huh?
	if ( ( $w['precision'] >= 9 and $w['year'] != $d['year'] ) or
	     ( $w['precision'] >= 10 and $w['month'] != $d['month'] ) or
	     ( $w['precision'] >= 11 and $w['day'] != $d['day'] ) ) {
		print "Dates differ\n" ;
		return false ;
	}
	return true ;
}

function scanDates ( $i , $prop , $the_date , $this_source ) {
	$dates = $i->getClaims($prop) ;
	foreach ( $dates AS $claim ) {
		if ( isDateSubset ( $claim , $the_date ) ) addSource ( $i , $claim->id , $this_source ) ;
	}
}

function getThisSource ( $catalog_item , $id , $source_prop ) {
	$this_source = array (
		array ( 'prop' => 'P248' , 'value' => $catalog_item , 'type' => 'wikibase-item' ) ,
		array ( 'prop' => 'P'.preg_replace('/\D/','',$source_prop) , 'value' => $id , 'type' => 'string' ) ,
		array ( 'prop' => 'P813' , 'value' => now() , 'type' => 'time' , 'precision' => '11' )
	) ;
	return $this_source ;
}

function sourceGND ( $i , $id , $source_prop ) {
	$this_source = getThisSource ( 'Q36578' , $id , $source_prop ) ;
	$url = "http://d-nb.info/gnd/$id/about/lds" ;
	$x = @file_get_contents ( $url ) ;
	if ( $x === false ) return ; // Nope
	$x = preg_replace ( '/\s+/' , ' ' , $x ) ;

	$claims = $i->getClaims ( 21 ) ;
	foreach ( $claims AS $claim ) {
		if ( $i->hasExternalSource($claim) ) continue ;
		$target = $i->getTarget($claim) ;
		if ( $target == 'Q6581097' and preg_match ( '|<gndo:gender rdf:resource="http://d-nb.info/standards/vocab/gnd/Gender#male" />|' , $x ) ) addSource ( $i , $claim->id , $this_source ) ;
		if ( $target == 'Q6581072' and preg_match ( '|<gndo:gender rdf:resource="http://d-nb.info/standards/vocab/gnd/Gender#female" />|' , $x ) ) addSource ( $i , $claim->id , $this_source ) ;
	}
	
	if ( preg_match_all ( '|<gndo:forename>(.+?)</gndo:forename>|' , $x , $m ) ) {
		$given_names = $i->getClaims(735) ;
		foreach ( $m[1] AS $label ) {
			tryStringValues ( $given_names , $i , $label , $this_source ) ;
		}
	}

	if ( preg_match_all ( '|<gndo:surname>(.+?)</gndo:surname>|' , $x , $m ) ) {
		$surnames = $i->getClaims(734) ;
		foreach ( $m[1] AS $label ) {
			tryStringValues ( $surnames , $i , $label , $this_source ) ;
		}
	}
	
	if ( preg_match ( '|<gndo:placeOfBirth>.*?<gndo:preferredNameForThePlaceOrGeographicName>(.+?)</gndo:preferredNameForThePlaceOrGeographicName>.*?</gndo:placeOfBirth>|' , $x , $m ) ) {
		$places = $i->getClaims(19) ;
		tryStringValues ( $places , $i , $m[1] , $this_source ) ;
	}

	if ( preg_match ( '|<gndo:placeOfDeath>.*?<gndo:preferredNameForThePlaceOrGeographicName>(.+?)</gndo:preferredNameForThePlaceOrGeographicName>.*?</gndo:placeOfDeath>|' , $x , $m ) ) {
		$places = $i->getClaims(20) ;
		tryStringValues ( $places , $i , $m[1] , $this_source ) ;
	}
	
	if ( preg_match ( '|<gndo:dateOfBirth rdf:datatype="http://www.w3.org/2001/XMLSchema#date">(\d+)-(\d+)-(\d+)</gndo:dateOfBirth>|' , $x , $m ) ) {
		scanDates ( $i , 569 , array ( 'year' => $m[1]*1 , 'month' => $m[2]*1 , 'day' => $m[3]*1 , 'precision' => 11 ) , $this_source ) ;
	} else if ( preg_match ( '|<gndo:dateOfBirth rdf:datatype="http://www.w3.org/2001/XMLSchema#gYear">(\d+)</gndo:dateOfBirth>|' , $x , $m ) ) {
		scanDates ( $i , 569 , array ( 'year' => $m[1]*1 , 'precision' => 9 ) , $this_source ) ;
	}

	if ( preg_match ( '|<gndo:dateOfDeath rdf:datatype="http://www.w3.org/2001/XMLSchema#date">(\d+)-(\d+)-(\d+)</gndo:dateOfDeath>|' , $x , $m ) ) {
		scanDates ( $i , 570 , array ( 'year' => $m[1]*1 , 'month' => $m[2]*1 , 'day' => $m[3]*1 , 'precision' => 11 ) , $this_source ) ;
	} else if ( preg_match ( '|<gndo:dateOfDeath rdf:datatype="http://www.w3.org/2001/XMLSchema#gYear">(\d+)</gndo:dateOfDeath>|' , $x , $m ) ) {
		scanDates ( $i , 570 , array ( 'year' => $m[1]*1 , 'precision' => 9 ) , $this_source ) ;
	}
}

function sourceIMDB ( $i , $id , $source_prop ) {
	$this_source = getThisSource ( 'Q37312' , $id , $source_prop ) ;
	$url = "http://www.imdb.com/name/$id/" ;
	$x = @file_get_contents ( $url ) ;
	if ( $x === false ) return ; // Nope
	$x = preg_replace ( '/\s+/' , ' ' , $x ) ;
/*	
	if ( preg_match ( '|<h1 class="header">\s*<span class="itemprop" itemprop="name">(.+?)</span>|' , $x , $m ) ) {
		$first_names = explode ( ' ' , $m[1] ) ;
		$last_name = array_pop ( $first_names ) ;
		
//		$names = $i->getClaims(20) ;
//		tryStringValues ( $places , $i , $m[1] , $this_source ) ;
	}
*/

	$months = array (
		1 => 'January' ,
		2 => 'February' ,
		3 => 'March' ,
		4 => 'April' ,
		5 => 'May' ,
		6 => 'June' ,
		7 => 'July' ,
		8 => 'August' ,
		9 => 'September' ,
		10 => 'October' ,
		11 => 'November' ,
		12 => 'December'
	) ;

	// Birth and death
	foreach ( array ( 569 , 570 ) AS $prop ) {
		$event = $prop == 569 ? 'born' : 'died' ;
		$claims = $i->getClaims ( $prop ) ;
		foreach ( $claims AS $claim ) {
			$v = $claim->mainsnak->datavalue->value ;
			if ( $v->calendarmodel != "http://www.wikidata.org/entity/Q1985727" ) continue ; // Gregorian only
			if ( !preg_match ( '|^\+0*(\d+)-(\d+)-(\d+)T|' , $v->time , $m ) ) continue ;
			$pattern = '' ;
			if ( $v->precision == 9 ) {
				$pattern = "($event on \S+ \d+, " . $m[1] . "|$event in " . $m[1] . ")" ;
			} else if ( $v->precision == 11 ) {
				$pattern = "$event on " . $months[$m[2]*1] . " " . ($m[3]*1) . ", " . $m[1] . "" ;
			} else continue ;
//			print "$prop: $pattern\n" ;
			if ( !preg_match ( "/$pattern/" , $x ) ) continue ;
			addSource ( $i , $claim->id , $this_source ) ;
		}
	}

}


function sourceYear ( $prop , $year , $i , $this_source ) {
	if ( trim($year) == '' ) return ; // Paranoia
	$claims = $i->getClaims ( $prop ) ;
	foreach ( $claims AS $claim ) {
		$v = $claim->mainsnak->datavalue->value ;
		if ( $v->calendarmodel != "http://www.wikidata.org/entity/Q1985727" ) continue ; // Gregorian only
		if ( $v->precision != 9 ) continue ; // Year only
		if ( !preg_match ( '|^\+0*'.$year.'-|' , $v->time ) ) continue ;
		addSource ( $i , $claim->id , $this_source ) ;
	}
}

function sourceYourPaintings ( $i , $id , $source_prop ) {
	$this_source = getThisSource ( 'Q7257339' , $id , $source_prop ) ;
	$url = "http://www.bbc.co.uk/arts/yourpaintings/artists/$id" ;
	$x = @file_get_contents ( $url ) ;
	if ( $x === false ) return ; // Nope
	$x = preg_replace ( '/\s+/' , ' ' , $x ) ;
	
	if ( preg_match ( '|<meta name="keywords" content=".+? Born: (\d+), Died: (\d+)" />|' , $x , $m ) ) {
		sourceYear ( 569 , $m[1] , $i , $this_source ) ;
		sourceYear ( 570 , $m[2] , $i , $this_source ) ;
	}
}

function sourceISNI ( $i , $id , $source_prop ) {
	$this_source = getThisSource ( 'Q423048' , $id , $source_prop ) ;
	$url = "http://isni.org/" . str_replace ( ' ' , '' , $id ) ;
	$x = @file_get_contents ( $url ) ;
	if ( $x === false ) return ; // Nope
	$x = preg_replace ( '/\s+/' , ' ' , $x ) ;
	
	$last_name = '' ;
	$first_names = array() ;
	$born = '' ;
	$died = '' ;
	
	if ( preg_match ( '|<title>ISNI \d+ ([^, ]+), (.+?) \((\d+)-(\d+)\)</title>|' , $x , $m ) ) {
		$last_name = $m[1] ;
		$first_names = explode ( ' ' , $m[2] ) ;
		$born = $m[3] ;
		$died = $m[4] ;
	}
	
	tryStringValues ( $i->getClaims(734) , $i , $last_name , $this_source ) ;
	foreach ( $first_names AS $n ) {
		tryStringValues ( $i->getClaims(735) , $i , $n , $this_source ) ;
	}
	sourceYear ( 569 , $born , $i , $this_source ) ;
	sourceYear ( 570 , $died , $i , $this_source ) ;
}


// DOES NOT WORK
// The French are weird.
function sourceBnF ( $i , $id , $source_prop ) {
	$this_source = getThisSource ( 'Q19938912' , $id , $source_prop ) ;
	$url = "http://catalogue.bnf.fr/ark:/12148/cb$id" ;
	$x = @file_get_contents ( $url ) ;
	print "$x\n" ;
	if ( $x === false ) return ; // Nope
	$x = preg_replace ( '/\s+/' , ' ' , $x ) ;
	
	
	if ( preg_match ( '|<td\s*class="texteNotice">(.+?)</td>|' , $x , $notice ) ) {
		$notice = $notice[1] ;
		if ( preg_match ( '|<b>\s*Naissance\s*:\s*</b>\s*(\d+)\s*<br />|' , $notice , $m ) ) sourceYear ( 569 , $m[1] , $i , $this_source ) ;
		if ( preg_match ( '|<b>\s*Mort\s*:\s*</b>\s*(\d+)\s*<br />|' , $notice , $m ) ) sourceYear ( 569 , $m[1] , $i , $this_source ) ;
	}

/* <b> Budd, Herbert Ashwin (1881-1950) </b> <br /><br /> 
<b> Nationalit&#233;(s) :  </b> Grande-Bretagne<br /> 
<b> Sexe :  </b> masculin<br /> 
<b> Responsabilit&#233;(s) exerc&#233;e(s) sur les documents :  </b> Auteur<br /> 
<b> Naissance :  </b> 1881<br /> 
<b> Mort :  </b> 1950<br /><br />
*/
}





function scanCatalog ( $i , $prop , $f ) {
	$claims = $i->getClaims ( $prop ) ;
	foreach ( $claims AS $claim ) $f ( $i , $claim->mainsnak->datavalue->value , $prop ) ;
}
// 
function scanItem ( $q ) {
	$i = new WDI ( $q ) ;
//	scanCatalog ( $i , 268 , 'sourceBnF' ) ; return;// DOES NOT WORK
	scanCatalog ( $i , 213 , 'sourceISNI' ) ;
	scanCatalog ( $i , 227 , 'sourceGND' ) ;
	scanCatalog ( $i , 345 , 'sourceIMDB' ) ;
	scanCatalog ( $i , 1367 , 'sourceYourPaintings' ) ;
}




function getAPI () {
	global $api ;
	if ( isset($api) and $api->isLoggedin() ) return $api ;
	$config = parse_ini_file ( '/data/project/wikidata-todo/source_bot.conf' ) ;
//print $config['user'] . " / " . $config['password'] ;
	$api = new \Mediawiki\Api\MediawikiApi( 'https://www.wikidata.org/w/api.php' );
	if ( !$api->isLoggedin() ) {
		$x = $api->login( new \Mediawiki\Api\ApiUser( $config['user'], $config['password'] ) );
		if ( !$x ) return false ;
	}
	return $api ;
}

function addSources () {
	global $sources ;
	$api = getAPI() ;
	foreach ( $sources AS $s ) {
		$action = 'wbsetreference' ;
		$params = array (
			'statement' => $s['claim'] ,
			'snaks' => json_encode ( $s['snaks'] ) ,
			'token' => $api->getToken() ,
			'summary' => '#botsource' ,
			'bot' => 1
		) ;
		try {
			$x = $api->postRequest( new \Mediawiki\Api\SimpleRequest( $action, $params ) );
		} catch (Exception $e) {
			echo 'Caught exception: ',  $e->getMessage(), "\n";
			$x = false ;
		}
	}
}

//scanItem ( 'Q42' ) ; // Douglas Adams
//scanItem ( 'Q4115189' ) ; // Sandbox item
//scanItem ( 'Q1000204' ) ;

//$q = 'Q4985246' ;



$db = new SQLite3 ( '/data/project/wikidata-todo/scripts/autosource/process.sqlite' ) ;
$testing = isset($q) ;
$sources = array() ;
while ( 1 ) {
	if ( !$testing ) {
		$sql = "SELECT min(done_with_version) AS min_version FROM items" ;
		$r = $db->querySingle ( $sql , true ) ;
		$min_version = $r['min_version'] ;
		if ( $min_version == $version ) break ; // All up-to-date
		
		$sql = "SELECT * FROM items WHERE done_with_version = $min_version ORDER BY random() LIMIT 1" ;
		$r = $db->querySingle ( $sql , true ) ;
		if ( count($r) == 0 ) break ;
		$q = 'Q' . $r['q'] ;
	}
	print "Scanning $q ...\n" ;

	$sources = array() ;
	scanItem ( $q ) ;
	
	if ( $testing ) {
		print_r ( $sources ) ;
		break ;
	} else {
		addSources () ;
		$sql = "UPDATE items SET done_with_version=$version WHERE q=" . $r['q'] ;
		$db->exec ( $sql ) ;
	}
	
}

if ( isset($api) ) $api->logout() ;
print "All done!\n" ;

// 2016-07-08 : 170292 edits

?>