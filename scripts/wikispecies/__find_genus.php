#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|

require_once ( '../../public_html/php/common.php' ) ;

$ranks = array (
	'subclass' => 'Q5867051' ,
	'infraphylum' => 'Q2361851' ,
	'genus' => 'Q34740' ,
	'superfamilia' => 'superfamilia' ,
	'subfamilia' => 'Q2455704' ,
	'ordo' => 'Q36602' ,
	'subclassis' => 'Q5867051' ,
	'familia' => 'Q35409' ,
	'classis' => 'Q37517' ,
	'subphylum' => 'Q1153785' ,
	'superclassis' => 'Q3504061' ,
	'superordo' => 'Q5868144'
) ;


$t = file_get_contents ( 'https://tools.wmflabs.org/wikidata-todo/duplicity.php?wiki=specieswiki&mode=list' ) ;
$t = preg_replace ( '/<t.*?>/' , "\t" , $t ) ;
$t = preg_replace ( '/<.+?>/' , '' , $t ) ;
$rows = explode ( "\n" , $t ) ;
$db = openDB ( 'wikidata' , 'wikidata' , true ) ;
foreach ( $rows AS $r ) {
	if ( !preg_match ( '/^\s+[0-9,]+\t(.+?)\tcheck.+$/' , $r , $m ) ) continue ;
	$title = $m[1] ;
	
	if ( !preg_match ( '/^[A-Z][a-z]+$/' , $title ) ) continue ;
	
	
	$sql = "select * from wb_items_per_site WHERE ips_site_id='specieswiki' and ips_site_page='$title'" ; # $title is just letters
	if(!$result = $db->query($sql)) {
		$db = openDB ( 'wikidata' , 'wikidata' , true ) ;
		if(!$result = $db->query($sql)) continue ; //die('There was an error running the query [' . $db->error . ']');
	}
	$found = false ;
	while($row = $result->fetch_assoc()) $found = 1 ;
	if ( $found ) continue ;
	
	
#q	print "\n-----\n\n$title\n" ;
	
	$qrank = '' ;
	$wiki = file_get_contents ( "https://species.wikimedia.org/w/index.php?title=$title&action=raw" ) ;

	if ( preg_match ( '/\{\{disambig\}\}/' , str_replace ( "\n" , ' ' , $wiki ) ) ) { // Disambiguation
/*		$items = getSPARQLitems ( "SELECT ?q { ?q wdt:P225 \"$title\" }" ) ;
		if ( count($items) == 0 ) {
			print "CREATE\n" ;
			print "LAST\tSspecieswiki\t\"$title\"\n" ;
			print "LAST\tLen\t\"$title\"\n" ;
			print "LAST\tP31\tQ4167410\n" ;
		} else {
			
		}*/
		continue ;
	}

	$rows = explode ( "\n" , $wiki ) ;
	foreach ( $rows AS $r ) {
		if ( !preg_match ( '/^[\*\# ]*([A-Z][a-z]+)\s*:{0,1}\s*\'*\[\['.$title.'\]\]/' , $r , $m ) ) continue ;
		$rank = strtolower ( $m[1] ) ;
		if ( isset($ranks[$rank]) ) {
			if ( $qrank == '' ) {
				$qrank = $ranks[$rank] ;
			} else {
				$qrank = "NO" ;
			}
		} else {
#			print "#No Q for $title as $rank\n" ;
		}
	}
	
	if ( $qrank == 'NO' or $qrank == '' ) continue ;
	
	$items = getSPARQLitems ( "SELECT ?q { ?q wdt:P225 \"$title\" . ?q wdt:P105 wd:$qrank }" ) ;
	if ( count($items) > 0 ) {
		if ( count ( $items ) == 1 ) {
			print "Q" . $items[0] . "\tSspecieswiki\t\"$title\"\n" ;
		}
		continue ;
	}
	
	print "CREATE\n" ;
	print "LAST\tSspecieswiki\t\"$title\"\n" ;
	print "LAST\tLen\t\"$title\"\n" ;
	print "LAST\tP225\t\"$title\"\n" ;
	print "LAST\tP31\tQ16521\n" ; // Taxon
	print "LAST\tP105\t$qrank\n" ;
	
}

?>