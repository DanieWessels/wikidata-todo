#!/usr/bin/php
<?PHP

# Gets Wikispecies-linked biographical Wikidata items, and tries to add some statements scraped from WS

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|

require_once ( '../../public_html/php/common.php' ) ;

$db = openDBwiki ( 'specieswiki' , true ) ;

#$parent_taxon = 'Q38348' ; # Phylum
$parent_taxon = 'Q36602' ; # Order
#$parent_taxon = 'Q35409' ; # Family

$t = file_get_contents ( 'https://tools.wmflabs.org/wikidata-todo/duplicity.php?wiki=specieswiki&mode=list' ) ;
$t = preg_replace ( '/<t.*?>/' , "\t" , $t ) ;
$t = preg_replace ( '/<.+?>/' , '' , $t ) ;
$rows = explode ( "\n" , $t ) ;
foreach ( $rows AS $r ) {
	if ( !preg_match ( '/^\s+[0-9,]+\t(.+?)\tcheck.+$/' , $r , $m ) ) continue ;
	$title = $m[1] ;
	if ( preg_match ( '/"/' , $title ) ) continue ; // No double quotes
	
//$title = 'Endothyranopsis' ;
	
	$sparql = "SELECT ?q ?parent ?article { ?q wdt:P225 \"$title\" . ?q wdt:P171* ?parent . ?parent wdt:P105 wd:$parent_taxon . ?article schema:about ?parent ; schema:isPartOf	<https://species.wikimedia.org/> }" ;
	$j = getSPARQL ( $sparql ) ;
	if ( !isset($j) or !isset($j->results) or !isset($j->results->bindings) ) continue ;
	$b = $j->results->bindings ;
	if ( count($b) == 0 ) continue ;
	$q = preg_replace ( '/^.+entity\/Q/' , 'Q' , $b[0]->q->value ) ;
	$phylum = $b[0]->article->value ;
	if ( !preg_match ( '/\/wiki\/(.+)$/' , $phylum , $m ) ) continue ;
	$phylum = str_replace ( ' ' , '_' , $m[1] ) ;
	
	$t2 = $db->real_escape_string ( str_replace(' ','_',$title) ) ;
	$p2 = $db->real_escape_string ( $phylum ) ;
	$sql = "SELECT * FROM page,pagelinks WHERE page_title='$t2' AND page_namespace=0 AND pl_from=page_id AND pl_namespace=0 AND pl_title='$p2'" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	$cnt = 0 ;
	while($o = $result->fetch_object()) $cnt++ ;
	if ( $cnt != 1 ) continue ;
	
	print "$q\tSspecieswiki\t\"$title\"\n" ;
}

?>