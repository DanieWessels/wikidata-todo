#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|

require_once ( '../../public_html/php/common.php' ) ;
require_once ( '../../public_html/php/wikidata.php' ) ;

$wil = new WikidataItemList ;
$db = openDB ( 'wikispecies' , 'wikispecies' ) ;

$qs = array() ;
$pages = array() ;
$sql = "select distinct page_title,pp_value from page,page_props,templatelinks where pp_page=page_id AND pp_propname='wikibase_item' AND page_id=tl_from AND tl_namespace=10 AND tl_from_namespace=0 AND tl_title IN ('VN','Vn')" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
while($o = $result->fetch_object()) {
	$pages[] = array ( $o->page_title , $o->pp_value ) ;
	$qs[] = $o->pp_value ;
}

$wil->loadItems ( $qs ) ;

$fn = '/data/project/wikidata-todo/scripts/wikispecies/vernacular.qs' ;
$fh = fopen ( $fn , 'w' ) ;

$source = "\tS143\tQ13679" ;
foreach ( $pages AS $pi ) {
	$page = $pi[0] ;
	$q = $pi[1] ;
//	$wil->loadItem ( $q ) ;
	$i = $wil->getItem ( $q ) ;
	if ( !isset($i) ) continue ;
	$claims = $i->getClaims ( 'P1843' ) ;

	$url = "https://species.wikimedia.org/w/index.php?title=".myurlencode($page)."&action=raw" ;
	$w = file_get_contents ( $url ) ;
	$w = preg_replace ( '/\s+/' , ' ' , $w ) ;
	if ( !preg_match ( '/\{\{\s*[Vv][Nn]\s*\|(.+?)\}\}/' , $w , $m ) ) continue ;
	$parts = explode ( '|' , $m[1] ) ;
	foreach ( $parts AS $p ) {
		$p = explode ( '=' , $p ) ;
		if ( count($p) != 2 ) continue ;
		$lang = trim ( $p[0] ) ;
		
		$names = explode ( ',' , $p[1] ) ;
		foreach ( $names AS $name ) {
			$name = trim ( $name ) ;
		
			$hadthat = false ;
			foreach ( $claims AS $c ) {
				if ( !isset($c->mainsnak) or !isset($c->mainsnak->datavalue) or !isset($c->mainsnak->datavalue->value) ) continue ;
				if ( $c->mainsnak->datavalue->value->language != $lang ) continue ;
				if ( strtolower($c->mainsnak->datavalue->value->text) != strtolower($name) ) continue ;
				$hadthat = true ;
				break ;
			}
			if ( $hadthat ) continue ;
		
			$cmd = "$q\tP1843\t$lang:\"$name\"$source" ;
			fwrite ( $fh , "$cmd\n" ) ;
		}
	}
}

fclose ( $fh ) ;

?>